import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch.nn import functional as F




def TT_metric_batchwise(device, seqs_1, seqs_2, seqs_1_lens, seqs_2_lens, length_, algo="ipot", order=1, penalty=20, beta=10, n_sink=5, n_ipot=1):
  """ 
  Calculate approximations of TT-metric between all pairs of sequences in two respective sequence
  batches and return the approximations in form of a matrix (TT_approx) containing for every pair of
  sequences their TT-metric approximation.

  Args:
    - seqs_1 (tensor): first batch of sequences
    - seqs_2 (tensor): second batch of sequences
    - seqs_1_lens (tensor): lengths of sequences in seqs_1
    - seqs_2_lens (tensor): lengths of sequences in seqs_2

  Parameters:
    - length_: cutoff length of sequences in both batches, set to the minimum of the two maximal
        sequence lengths in the two batches, ensures that seqs_1 and seqs_2 tensors are of same size
    - algo: approximate TT-metric with either the Sinkhorn algorithm or the IPOT algorithm
    - order: order of the TT-metric
    - penalty: penalty parameter in the TT-metric
    - beta: regularization parameter in Sinkhorn or IPOT algorithm
    - n_sink: number of Sinkhorn iterations, corresponds to inner iterations if algo = "ipot"
    - n_ipot: number of main iterations in IPOT algorithm, ignored when algo = "sink"

  Return:
    - T (tensor): tensor which contains for every pair of sequences in seqs_1 and seqs_2 the cost 
        matrix that corresponds to the primal transport formulation of the TT-metric
    - T (tensor): tensor which contains for every pair of sequences in seqs_1 and seqs_2 the
        transport plan approximation that corresponds to the solution of the primal transport
        formulation of the TT-metric
    - TT_approx (tensor): TT-metric approximation for every pair of sequences in seqs_1 and seqs_2

  """

  X = seqs_1
  Y = seqs_2

  batch_size = X.shape[0]  # number of sequences in each batch

  X_len = seqs_1_lens
  Y_len = seqs_2_lens

  X = X.squeeze()
  Y = Y.squeeze()

  X = X[:, :length_]
  Y = Y[:, :length_]

  X = X.unsqueeze(1) # (batch_size, 1         , length)
  X = X.unsqueeze(3) # (batch_size, 1         , length, 1)
  Y = Y.unsqueeze(0) # (1         , batch_size, length)
  Y = Y.unsqueeze(2) # (1         , batch_size, 1     , length)

  C = torch.abs(X - Y)


  # pad sequences with penalty value
  length_arange_vector = torch.arange(length_, dtype=torch.float64).to(device)
  
  mask_X = length_arange_vector.expand(len(X_len), length_) < X_len.unsqueeze(1)
  mask_Y = length_arange_vector.expand(len(Y_len), length_) < Y_len.unsqueeze(1)

  mask_X = mask_X.unsqueeze(1)              # (batch_size, 1, length)
  mask_X = mask_X.unsqueeze(2)              # (batch_size, 1, 1     , length)
  mask_X = torch.transpose(mask_X, -2, -1)  # (batch_size, 1, length, 1)

  mask_Y = mask_Y.unsqueeze(0)   # (1, batch_size, length)
  mask_Y = mask_Y.unsqueeze(2)   # (1, batch_size, 1     , length)

  mask = mask_X * mask_Y # 1 where index of sequence element smaller than sequence length 0 else

  mask_penalty = torch.logical_not(mask) * penalty 

  C = C * mask
  C += mask_penalty


  C = torch.abs(C) ** order

  T = torch.ones((batch_size, batch_size, length_, length_), requires_grad=True).to(device)
  b = torch.ones((batch_size, batch_size, length_, 1), requires_grad=True).to(device)
  A = torch.exp(-C/beta)

  length_float = 1.0 * length_

  if algo == "sink":
    Q = A
    QT = torch.transpose(Q, 2, 3)

    for i in range(n_sink):
      a = 1/(length_float * torch.matmul(Q, b))
      b = 1/(length_float * torch.matmul(QT, a))

    a_diag = torch.diag_embed(a.squeeze(), dim1=-2, dim2=-1)
    b_diag = torch.diag_embed(b.squeeze(), dim1=-2, dim2=-1)

    T = torch.matmul(a_diag, Q)
    T = torch.matmul(T, b_diag)

  if algo == "ipot":
    for i in range(n_ipot):
      Q = A*T
      QT = torch.transpose(Q, 2, 3)

      for j in range(n_sink):
        a = 1/(length_float * torch.matmul(Q, b))
        b = 1/(length_float * torch.matmul(QT, a))

      a_diag = torch.diag_embed(a.squeeze(), dim1=-2, dim2=-1)
      b_diag = torch.diag_embed(b.squeeze(), dim1=-2, dim2=-1)

      T = torch.matmul(a_diag, Q)
      T = torch.matmul(T, b_diag)

  TT_approx = T * C
  TT_approx = torch.sum(TT_approx, dim=(-2,-1))
  TT_approx = (length_float * TT_approx) ** (1/order)

  return T, C, TT_approx

def xiao_metric_batchwise(device, seqs_1, seqs_2, seqs_1_lens, seqs_2_lens, max_T, length_, reduced_pairing=False):
  """ 
  Calculate Xiao distances between two batches of sequences and return the distances in form of a
  matrix (dist) containing for every pair of sequences their Xiao-metric approximation.

  Args:
    - seqs_1 (tensor): first batch of sequences
    - seqs_2 (tensor): second batch of sequences
    - seqs_1_lens (tensor): lengths of sequences in seqs_1
    - seqs_2_lens (tensor): lengths of sequences in seqs_2

  Parameters:
    - length_: cutoff length of sequences in both batches, set to the minimum of the two maximal
        sequence lengths in the two batches, ensures that seqs_1 and seqs_2 are of same size
    - reduced_pairing: if true only calculate distances between sequence pairs with same index in
        respective batch (done in baseline model) if false calculate distances between all pairs of 
        sequences in batch

  Return:
    - dist: xiao distances between the two batches of sequences

  """

  X = seqs_1
  Y = seqs_2

  X_len = seqs_1_lens
  Y_len = seqs_2_lens

  X = X.squeeze()
  Y = Y.squeeze()

  X = X[:, :length_]
  Y = Y[:, :length_]

  length_arange_vector = torch.arange(length_, dtype=torch.float64).to(device)
  
  maskX = length_arange_vector.expand(len(X_len), length_) < X_len.unsqueeze(1)
  maskY = length_arange_vector.expand(len(Y_len), length_) < Y_len.unsqueeze(1)

  X = X * maskX
  Y = Y * maskY

  X = X + torch.logical_not(maskX) * max_T
  Y = Y + torch.logical_not(maskY) * max_T

  if not reduced_pairing:
    X = X.unsqueeze(1) # (batch_size, 1         , length)
    Y = Y.unsqueeze(0) # (1         , batch_size, length)

  dist = torch.abs(X - Y)
  dist = torch.sum(dist, dim=-1)

  return dist

def wasserstein_primal(device, C, algo="ipot", beta=10, n_sink=1, n_ipot=5):
  """ 
  Calculate primal Wasserstein distance between sample distributions of the sequence batches based
  on the cost matrix that contains their pairwise distances (according to xiao-metric or TT-metric).

  Args:
    - C: cost matrix containing pairwise distances of two sequence batches

  Parameters:
    - algo: solve Wasserstein primal with Sinkhorn distance or IPOT algorithm
    - beta: regularization parameter in Sinkhorn distance or IPOT algorithm
    - n_sink: number of Sinkhorn iterations, corresponds to inner iterations if approx = "ipot"
    - n_ipot: number of main iterations in IPOT algorithm, ignored when approx = "sink"

  Return:
    - dist: approximation of exact Wasserstein distance if approx = "ipot" and regularized
        Wasserstein distance if approx = "sink" (Sinkhorn distance)

  """

  L = C.shape[0] # batch size
  T = torch.ones((L, L), requires_grad=True).to(device)
  b = torch.ones((L), requires_grad=True).to(device)
  A = torch.exp(-C/beta)

  if algo == "sink":
    Q = A
    QT = torch.transpose(Q, -2, -1)

    for i in range(n_sink):
      a = 1/(L * torch.matmul(Q, b))
      b = 1/(L * torch.matmul(QT, a))

    a_diag = torch.diag_embed(a.squeeze(), dim1=-2, dim2=-1)
    b_diag = torch.diag_embed(b.squeeze(), dim1=-2, dim2=-1)

    T = torch.matmul(a_diag, Q)
    T = torch.matmul(T, b_diag)

  if algo == "ipot":
    for i in range(n_ipot):
      Q = A*T
      QT = torch.transpose(Q, -2, -1)

      for j in range(n_sink):
        a = 1/(L * torch.matmul(Q, b))
        b = 1/(L * torch.matmul(QT, a))

      a_diag = torch.diag_embed(a.squeeze(), dim1=-2, dim2=-1)
      b_diag = torch.diag_embed(b.squeeze(), dim1=-2, dim2=-1)

      T = torch.matmul(a_diag, Q)
      T = torch.matmul(T, b_diag)

  dist = T*C
  dist = torch.sum(dist, dim=(-2, -1))

  return dist


class Generator(nn.Module):
  """ 
  LSTM based neural network responsible for generating sequences in all three main models by
  transforming a batch of noise sequences into a batch of new sequences.

  Args:
    - x, x_len: batch of noise sequences together with their lengths

  Parameters:
    - hidden_dim: size of hidden layer in the LSTM
    - dropout_p: use a dropout layer in the neural network with this dropout probability

  Return:
    - x, x_len: transformed noise sequences together with their lengths
  """

  def __init__(self, hidden_dim=64, dropout_p=0.1):
    super(Generator, self).__init__()

    self.hidden_dim = hidden_dim
    self.dropout_p = dropout_p

    self.lstm = nn.LSTM(1, hidden_dim, num_layers=1, batch_first=True)
    self.linear = nn.Linear(hidden_dim, 1)
    if dropout_p==None:
      self.variable_layer = nn.Identity()
    else:
      self.variable_layer = nn.Dropout(dropout_p)

  def forward(self, x, x_len):
    x    = torch.nn.utils.rnn.pack_padded_sequence(x, x_len.tolist(), batch_first=True)
    x, _ = self.lstm(x)
    x, _ = torch.nn.utils.rnn.pad_packed_sequence(x, batch_first=True)
    x    = self.variable_layer(x)
    x    = self.linear(x)
    x    = F.elu(x) + 1
    return x, x_len

class Discriminator(nn.Module):
  """ 
  Neural network in the WGAN based model responsible for assigning a score to a batch of sequences 
  (observed or generated). This in turn is used to calculate the Wasserstein distance between the 
  sample distributions of the sequence batches based on the dual formulation of Wasserstein distance.

  Args (in forward method):
    - x, x_len: batch of sequences together with their lengths

  Parameters:
    - hidden_dim: size of hidden layer in the LSTM
    - dropout_p: if specified use a dropout layer in the neural network with this dropout probability

  Return (in forward method):
    - val: assigned score to the batch of sequences

  """

  def __init__(self, device, hidden_dim=64, dropout_p=None):
    super(Discriminator, self).__init__()
    self.device = device
    self.hidden_dim = hidden_dim

    self.lstm = nn.LSTM(1, hidden_dim, num_layers=1, batch_first=True)
    self.dropout = nn.Dropout(p=dropout_p)
    self.linear = nn.Linear(hidden_dim, 1)
    if dropout_p==None:
      self.variable_layer = nn.Identity()
    else:
      self.variable_layer = nn.Dropout(dropout_p)
    
  def forward(self, x, x_len):
    seqlen = x.shape[1]
    length_arange_vector = torch.arange(seqlen, dtype=torch.float64).to(self.device)
    maskX = length_arange_vector.expand(len(x_len), seqlen) < x_len.unsqueeze(1)

    x    = torch.nn.utils.rnn.pack_padded_sequence(x, x_len.tolist(), batch_first=True)
    x, _ = self.lstm(x)
    x, _ = torch.nn.utils.rnn.pad_packed_sequence(x, batch_first=True)
    x    = self.variable_layer(x)
    x    = self.linear(x)
    x    = x.squeeze()
    x   *= maskX

    val = torch.sum(x, axis=1)
    val /= torch.sum(maskX, axis=1)

    return val

class WGAN(object):  
  """ 
  Main class that implements and trains the WGAN based generative model. The generator network and the 
  discriminator network are instantiated by the Generator and Discriminator class respectively. Both
  neural networks are trained by iterating over the batches of observed sequences and noise 
  sequences using the learning scheme of a WGAN. The model only uses the Xiao metric.

  Args (in train method):
    - ITERS: training iterations of the generator
    - n_discr: number of training iterations of discriminator per generator training iteration

  Parameters:
    - hidden_dim: size of hidden layer in the LSTM of both the generator and the discriminator
    - dropout_p: if specified use a dropout layer in both the generator and the discriminator with
         this dropout probability
    - learn_rate: learning rate in the gradient-descent based optimization in both the generator
        and the discriminator
    - lip_reg: if True (default) use the Lipschitz penality for regularization in the WGAN model
    - LAMBDA_LP: regularization parameter of the regularization

  """

  def __init__(self,
               device,
               hidden_dim=64,
               dropout_p=0.1,
               learn_rate=0.001,
               n_discr=15,
               LAMBDA_LP=0.1,
               lip_reg=True):

    self.device = device
    self.hidden_dim = hidden_dim
    self.dropout_p = dropout_p
    self.learn_rate = learn_rate
    self.n_discr = n_discr
    self.lip_reg = lip_reg
    self.LAMBDA_LP = LAMBDA_LP

    self.generator = Generator(hidden_dim=self.hidden_dim, dropout_p=self.dropout_p)
    self.discriminator = Discriminator(device=device, hidden_dim=self.hidden_dim, dropout_p=self.dropout_p)

    self.G_optimizer = optim.Adam(self.generator.parameters(), lr=self.learn_rate, betas=(0.9, 0.99))
    self.D_optimizer = optim.Adam(self.discriminator.parameters(), lr=self.learn_rate, betas=(0.9, 0.99))

    self.generator.to(device)
    self.discriminator.to(device)

  def train(self, ITERS, iterator_real, iterator_noise, max_T, report_every=100):
    # G_loss_history = []

    for i in range(ITERS):

      # Training steps of discriminator
      for _ in range(self.n_discr):
        xi, xi_len = iterator_noise.next_batch()
        eta, eta_len = iterator_real.next_batch()

        length_ = min(np.size(xi, 1), np.size(eta, 1))

        xi = torch.Tensor(xi).to(self.device)
        eta = torch.Tensor(eta).to(self.device)

        xi_len = torch.Tensor(xi_len).to(self.device)
        eta_len = torch.Tensor(eta_len).to(self.device)

        self.D_optimizer.zero_grad()

        gen_seq, gen_seq_len = self.generator(xi, xi_len)

        D_fake = self.discriminator(gen_seq, gen_seq_len)
        D_real = self.discriminator(eta, eta_len)

        D_loss = torch.mean(D_fake) - torch.mean(D_real)

        length_0 = np.minimum(gen_seq.shape[1], eta.shape[1])

        if self.lip_reg:
          seqs_dist = xiao_metric_batchwise(self.device, gen_seq, eta, gen_seq_len, eta_len, max_T, length_0, reduced_pairing=True)

          lip_div = torch.abs(D_real-D_fake)/torch.sqrt(seqs_dist+0.00001)
          lip_div = torch.mean((lip_div-1)**2)

          D_loss += self.LAMBDA_LP * lip_div
          reg_pen = lip_div

        D_loss.backward()
        self.D_optimizer.step()

      # Training step of generator
      for _ in range(1):
        xi, xi_len = iterator_noise.next_batch()

        xi = torch.Tensor(xi).to(self.device)
        xi_len = torch.Tensor(xi_len).to(self.device)

        self.G_optimizer.zero_grad()

        gen_seq, gen_seq_len = self.generator(xi, xi_len)

        D_fake = self.discriminator(gen_seq, gen_seq_len)

        G_loss = -torch.mean(D_fake)

        G_loss.backward()
        self.G_optimizer.step()

      if (i+1)%report_every == 0:
        # G_loss_history.append(float(G_loss))
        # moving_avg_G_loss = np.mean(G_loss_history[-5:]) # 5-step moving average

        # print('Training Progress: {:>6d} of {:>6d} || G_loss: {:>8.3f} || G_loss (5-step moving avg): {:>8.3f} || D_loss: {:>8.3f}'.format(i+1, ITERS, G_loss, moving_avg_G_loss, D_loss))
        print('Training Progress: {:>6d} of {:>6d} || G_loss: {:>8.3f} || D_loss: {:>8.3f}'.format(i+1, ITERS, G_loss, D_loss))

class Sinkhorn(object): 
  """ 
  Main class that implements and trains the Sinkhorn distance based generative model. The generator 
  network is instantiated by the Generator class. The generator network is trained with respect to 
  loss function given by the wasserstein_primal that calculates the Sinkhorn distance between sample
  distributions of the generated sequences and the observed sequences.

  Args (in train method):
    - ITERS: training iterations of the generator
    - n_ipot_TT: number of main iterations in IPOT approximation of TT-metric
    - n_sink_TT: number of Sinkhorn iterations in Sinkhorn or IPOT approximation of TT-metric
    - beta_TT: regularization parameter in Sinkhorn or IPOT approximation of TT-metric
    - algo_TT: use Sinkhorn or IPOT approximation of TT-metric

  Parameters:
    - hidden_dim: size of hidden layer in the LSTM of the generator
    - dropout_p: if specified use a dropout layer in the generator with this dropout probability
    - learn_rate: learning rate in the gradient-descent based optimization of the generator
    - n_sink: number of Sinkhorn iterations used for calculating the Sinkhorn distance
    - beta: regularization parameter in the Sinkhorn distance calculation
    - metric: use the Xiao-metric or the TT-metric as metric for the sequences

  """ 

  def __init__(self,
               device,
               hidden_dim=64,
               dropout_p=0.1,
               learn_rate=0.001,
               n_sink=10,
               metric='TT',
               beta=50):

    self.device = device
    self.hidden_dim = hidden_dim
    self.dropout_p = dropout_p
    self.learn_rate = learn_rate
    self.n_sink = n_sink
    self.metric = metric
    self.beta = beta

    self.generator = Generator(hidden_dim=self.hidden_dim, dropout_p=self.dropout_p)

    self.G_optimizer = optim.Adam(self.generator.parameters(), lr=self.learn_rate, betas=(0.9, 0.99))

    self.generator.to(device)

  def train(self, ITERS, iterator_real, iterator_noise, max_T, report_every=100,
            n_sink_TT=1, n_ipot_TT=10, beta_TT=None, algo_TT='ipot'):
    
    if beta_TT == None:
      beta_TT = max_T/20

    G_loss_history = []

    for i in range(ITERS):
      # Training step of generator
    
      noise_seqs, noise_seqs_lens = iterator_noise.next_batch()
      real_seqs, real_seqs_lens = iterator_real.next_batch()

      length_ = min(np.size(noise_seqs, 1), np.size(real_seqs, 1))

      noise_seqs = torch.Tensor(noise_seqs).to(self.device)
      real_seqs = torch.Tensor(real_seqs).to(self.device)

      noise_seqs_lens = torch.Tensor(noise_seqs_lens).to(self.device)
      real_seqs_lens = torch.Tensor(real_seqs_lens).to(self.device)

      self.generator.zero_grad()

      gen_seqs, gen_seqs_lens = self.generator(noise_seqs, noise_seqs_lens)

      if self.metric == 'TT':
        C = TT_metric_batchwise(self.device, gen_seqs, real_seqs, gen_seqs_lens, real_seqs_lens,
                      length_=length_,
                      algo=algo_TT,
                      order=1,
                      penalty=max_T/2,
                      beta=beta_TT,
                      n_sink=n_sink_TT,
                      n_ipot=n_ipot_TT)[2]
      else:
        C = xiao_metric_batchwise(self.device, gen_seqs, real_seqs, gen_seqs_lens, real_seqs_lens, max_T, length_=length_)

      loss = wasserstein_primal(self.device, C, algo="sink", beta=self.beta, n_sink=self.n_sink)
      loss.backward()

      self.G_optimizer.step()

      if (i+1)%report_every == 0:
        G_loss_history.append(float(loss))
        moving_avg_G_loss = np.mean(G_loss_history[-5:]) # 5-step moving average

        print('Training Progress: {:>6d} of {:>6d} || G_loss: {:>8.3f} || G_loss (5-step moving avg): {:>8.3f}'.format(i+1, ITERS, loss, moving_avg_G_loss))

class IPOT(object):
  """ 
  Main class that implements and trains the IPOT based generative model. The generator network is 
  instantiated by the Generator class. The generator network is trained with respect to 
  loss function given by the wasserstein_primal that calculates the Wasserstein distance between
  sample distributions of the generated sequences and the observed sequences based on the IPOT
  algorithm.

  Args (in train method):
    - ITERS: training iterations of the generator
    - n_ipot_TT: number of main iterations in IPOT approximation of TT-metric
    - n_sink_TT: number of Sinkhorn iterations in Sinkhorn or IPOT approximation of TT-metric
    - beta_TT: regularization parameter in Sinkhorn or IPOT approximation of TT-metric
    - algo_TT: use Sinkhorn or IPOT approximation of TT-metric

  Parameters:
    - hidden_dim: size of hidden layer in the LSTM of the generator
    - dropout_p: if specified use a dropout layer in the generator with this dropout probability
    - learn_rate: learning rate in the gradient-descent based optimization of the generator
    - n_ipot: number of main (outer) iterations in the IPOT algorithm for calculating the
        Wasserstein distance between sequence batches
    - n_sink: number of Sinkhorn (inner) iterations in the IPOT algorithm
    - beta: regularization parameter in the IPOT algorithm
    - metric: use the Xiao-metric or the TT-metric as metric for the sequences

  """ 

  def __init__(self,
               device,
               hidden_dim=64,
               dropout_p=0.1,
               learn_rate=0.001,
               n_sink=1,
               n_ipot=10,
               metric='TT',
               beta=50):

    self.device = device
    self.hidden_dim = hidden_dim
    self.dropout_p = dropout_p
    self.learn_rate = learn_rate
    self.n_sink = n_sink
    self.n_ipot = n_ipot
    self.metric = metric
    self.beta = beta

    self.generator = Generator(hidden_dim=self.hidden_dim, dropout_p=self.dropout_p)

    self.G_optimizer = optim.Adam(self.generator.parameters(), lr=self.learn_rate, betas=(0.9, 0.99))

    self.generator.to(device)

  def train(self, ITERS, iterator_real, iterator_noise, max_T, report_every=100,
            n_sink_TT=1, n_ipot_TT=10, beta_TT=None, algo_TT='ipot'):

    if beta_TT == None:
      beta_TT = max_T/20

    G_loss_history = []

    for i in range(ITERS):
      # Training step of generator
    
      noise_seqs, noise_seqs_lens = iterator_noise.next_batch()
      real_seqs, real_seqs_lens = iterator_real.next_batch()

      length_ = min(np.size(noise_seqs, 1), np.size(real_seqs, 1))

      noise_seqs = torch.Tensor(noise_seqs).to(self.device)
      real_seqs = torch.Tensor(real_seqs).to(self.device)

      noise_seqs_lens = torch.Tensor(noise_seqs_lens).to(self.device)
      real_seqs_lens = torch.Tensor(real_seqs_lens).to(self.device)

      self.generator.zero_grad()

      gen_seqs, gen_seqs_lens = self.generator(noise_seqs, noise_seqs_lens)

      if self.metric == 'TT':
        C = TT_metric_batchwise(self.device, gen_seqs, real_seqs, gen_seqs_lens, real_seqs_lens,
                      length_=length_,
                      algo=algo_TT,
                      order=1,
                      penalty=max_T/2,
                      beta=beta_TT,
                      n_sink=n_sink_TT,
                      n_ipot=n_ipot_TT)[2]
      else:
        C = xiao_metric_batchwise(self.device, gen_seqs, real_seqs, gen_seqs_lens, real_seqs_lens, max_T, length_=length_)

      loss = wasserstein_primal(self.device, C, algo="ipot", beta=self.beta, n_sink=self.n_sink, n_ipot=self.n_ipot)
      loss.backward()

      self.G_optimizer.step()

      if (i+1)%report_every == 0:
        G_loss_history.append(float(loss))
        moving_avg_G_loss = np.mean(G_loss_history[-5:]) # 5-step moving average

        print('Training Progress: {:>6d} of {:>6d} || G_loss: {:>8.3f} || G_loss (5-step moving avg): {:>8.3f}'.format(i+1, ITERS, loss, moving_avg_G_loss))