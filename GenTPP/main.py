import numpy as np
import sympy as sy
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from torch.nn import functional as F

import math
import argparse
from itertools import combinations

from utils import intens_func_from_gauss_params, retrieve_seqs_from_gen
from model import WGAN, Sinkhorn, IPOT
from analysis import plot_diagnostics, plot_sample_gen_real_noise
from data import (create_hom_poisson,
                  create_hawkes,
                  create_inhom_poisson,
                  create_inhom_poisson_gauss_kernels,
                  IntensitySumGaussianKernel,
                  CreateValidationData,
                  PaddedDataIterator)




parser = argparse.ArgumentParser(description='Generative modelling for temporal point processes.')
parser.add_argument('--gpu', default=False, help="Use GPU if available.")

parser.add_argument('--DATA', default='inhom_poisson_gauss_kernels', help="Type of process to use for real data. Options: 'hawkes', 'inhom_poisson_gauss_kernels'.")
parser.add_argument('--BATCH_SIZE', type=int, default=32, help="Training batch size.")

parser.add_argument('--max_T', type=float, default=20, help="End point of time interval that the sequences are defined on.")
parser.add_argument('--SEQ_NUM', type=int, default=2000, help="Number of sequences to simulate respectively in real process and noise process.")

parser.add_argument('--hawkes_alpha', type=float, default=0.9, help="Magnitude of off-spring process.")
parser.add_argument('--hawkes_mu', type=float, default=0.2, help="Background intensity.")
parser.add_argument('--hawkes_omega', type=float, default=1.0, help="Magnitude of exponential decay.")

parser.add_argument('--gauss_coefs', type=str, default='5, 7, 5', help="Magnitudes for the gaussian kernels, e.g. 5,5.")
parser.add_argument('--gauss_centers', type=str, default='5, 10, 15', help="Centers for the gaussian kernels, e.g. 10,20.")
parser.add_argument('--gauss_stds', type=str, default='1, 1, 1', help="Standard deviations the for gaussian kernels, e.g. 1,1.")

parser.add_argument('--model_type', default='IPOT', help="Type of generative model to use. Options: 'WGAN', 'Sinkhorn', 'IPOT.")

parser.add_argument('--ITERS', type=int, default=2000, help="Number of training iterations for the generative model.")
parser.add_argument('--hidden_dim', type=int, default=64, help="Dimension of hidden layers in the LSTMs.")
parser.add_argument('--dropout_p', type=float, default=0.1, help="Dropout probability in dropout layers.")
parser.add_argument('--learn_rate', type=float, default=0.001, help="Learning rate in the training of generative model.")

parser.add_argument('--metric', default='xiao', help="Type of sequenec metric to use (Xiao-metric or TT-metric). Options: 'xiao', 'TT'.")
parser.add_argument('--n_discr', type=int, default=10, help="Number of training iterations of discriminator per generator training iteration in WGAN model.")
parser.add_argument('--LAMBDA_LP', type=float, default=0.1, help="Regularization parameter in Lipschitz regularization of WGAN model.")
parser.add_argument('--beta', type=float, default=100, help="Regularization parameter in Sinkhorn or IPOT model.")
parser.add_argument('--n_sink', type=int, default=1, help="Number of Sinkhorn iterations in Sinkhorn model or IPOT model.")
parser.add_argument('--n_ipot', type=int, default=10, help="Number of main iterations in IPOT model.")


opt = parser.parse_args()

use_cuda = opt.gpu
use_cuda = False if not use_cuda else torch.cuda.is_available()
device = torch.device('cuda:0' if use_cuda else 'cpu')
torch.cuda.get_device_name(device) if use_cuda else 'cpu'

if use_cuda:
  print("Using GPU for acccelerated training.")
else:
  print("GPU not available. Using CPU.")

sequences_real = []

params_hawkes = None
intens_func = None
gauss_params = None

show_intens_func = True

gauss_coefs = [float(s) for s in opt.gauss_coefs.split(',')]
gauss_centers = [float(s) for s in opt.gauss_centers.split(',')]
gauss_stds = [float(s) for s in opt.gauss_stds.split(',')]



## simulate real data
if opt.DATA == 'hawkes':
  print("--- Simulate real data: Hawkes process")
  params_hawkes = {'alpha': opt.hawkes_alpha,'mu': opt.hawkes_mu, 'omega': opt.hawkes_omega}
  sequences_real = create_hawkes(params = params_hawkes,
                                 n_samples=opt.SEQ_NUM,
                                 max_T=opt.max_T,
                                 min_seq_len=2)


if opt.DATA == 'inhom_poisson_gauss_kernels':
  print("--- Simulate real data: inhom. Poisson process")
  gauss_params = dict()
  gauss_params['coef'] = gauss_coefs
  gauss_params['center'] = gauss_centers
  gauss_params['std'] = gauss_stds

  n_kernels = len(gauss_params['coef'])
  max_height_gauss_kernels = 0.15

  intens_gauss_kernels = IntensitySumGaussianKernel(k=n_kernels, 
                                                    centers=gauss_params['center'],
                                                    stds=gauss_params['std'],
                                                    coefs=gauss_params['coef'])
  
  intens_func = intens_func_from_gauss_params(gauss_params)
  
  sequences_real = create_inhom_poisson_gauss_kernels(intens_gauss_kernels,
                                                      n_samples=opt.SEQ_NUM,
                                                      max_T=opt.max_T)



## simulate noise data
print("--- Simulate noise data: hom. Poisson process")
lambda0 = np.mean([len(item) for item in sequences_real])/opt.max_T
sequences_noise = create_hom_poisson(mean=lambda0,
                                     n_samples=opt.SEQ_NUM,
                                     max_T=opt.max_T)



## initialize data iterators for batch-wise training
iterator_noise = PaddedDataIterator(sequences_noise, opt.BATCH_SIZE, pad_elem = -1, sort_by_len=True)
iterator_real  = PaddedDataIterator(sequences_real, opt.BATCH_SIZE, pad_elem = -1, sort_by_len=True)



## print some summary statistics about the data
max_seq_len = max([len(seq) for seq in sequences_noise+sequences_real])
avg_seq_len = np.round(np.mean([len(item) for item in sequences_real]), 2)



## validation data that contains noise tensors which we can evaluate the generator on
n_val_seqs = int(1000/(avg_seq_len)) # so that we have roughly 1000 points in all validation sequences combined
val_data = CreateValidationData(sequences_noise, sequences_real, n_seqs=n_val_seqs, device=device)

print("\n--- Number of sequences (with length > 2): {:d} (real process), {:d} (noise process)".format(len(sequences_real), len(sequences_noise)))
print("--- Maximal sequence length of real and noise process combined: {:d}".format(max_seq_len))
print("--- Average number of points per real sequence: ", avg_seq_len)



## Instantiate model
if opt.model_type == "WGAN":
  print("\n--- Create generative model. (type: WGAN, learn rate: {:s}, reg. param: {:s}, n_discr: {:d})".format(str(opt.learn_rate), str(opt.LAMBDA_LP), str(opt.n_discr)))
  model = WGAN(device=device,
               n_discr=opt.n_discr,
               hidden_dim=opt.hidden_dim,
               learn_rate=opt.learn_rate,
               lip_reg = True,
               LAMBDA_LP = opt.LAMBDA_LP)

if opt.model_type == "Sinkhorn":
  print("\n--- Create generative model. (type: Sinkhorn, learn rate: {:s}, reg. param: {:s}, n_sink: {:d}, metric: {:s})".format(str(opt.learn_rate), str(opt.beta), opt.n_sink, opt.metric))
  model = Sinkhorn(device=device,
                   hidden_dim=opt.hidden_dim,
                   dropout_p=opt.dropout_p,
                   learn_rate=opt.learn_rate,
                   n_sink=opt.n_sink,
                   beta=opt.beta,
                   metric=opt.metric)

if opt.model_type == "IPOT":
  print("\n--- Create generative model. (type: IPOT, learn rate: {:s}, reg. param: {:s}, n_sink: {:d}, n_ipot: {:d}, metric: {:s})".format(str(opt.learn_rate), str(opt.beta), opt.n_sink, opt.n_ipot, opt.metric))
  model = IPOT(device=device,
               hidden_dim=opt.hidden_dim,
               dropout_p=opt.dropout_p,
               learn_rate=opt.learn_rate,
               n_sink=opt.n_sink,
               n_ipot=opt.n_ipot,
               beta=opt.beta,
               metric=opt.metric)



## Train model
print("--- Train the model:")
model.train(ITERS=opt.ITERS,
            report_every=100,
            iterator_real=iterator_real,
            iterator_noise=iterator_noise,
            max_T=opt.max_T)

print("--- Training finished.")


## residual analysis and sample of generated sequences
generator = model.generator

gen_seqs = retrieve_seqs_from_gen(val_data.batch_noise_seqs, val_data.batch_noise_seqs_lens, generator)

plot_diagnostics(gen_seqs,
                 data_type=opt.DATA,
                 max_T=opt.max_T,
                 params_hawkes=params_hawkes,
                 gauss_params=gauss_params,
                 intens_func=intens_func,
                 show_intens_func=True,
                 n_bins=12,
                 size=4)

gen_seqs = retrieve_seqs_from_gen(val_data.batch_noise_seqs,
                                  val_data.batch_noise_seqs_lens,
                                  generator)

plot_sample_gen_real_noise(seqs_gen=gen_seqs,
                           seqs_real=val_data.seqs_real,
                           seqs_noise=val_data.seqs_noise,
                           intens_func=intens_func,
                           plot_intens_func=show_intens_func,
                           max_T=opt.max_T,
                           n_samples=10,
                           size=4)