# Generative Modelling For Temporal Point Processes

Code accompanying the thesis project "Wasserstein learning for generative point process models". The project was to assess neural network based generative models for temporal point processes that use the Wasserstein distance as the objective loss function for the training of the networks. More specifically, the generative models consist each of a generator neural network that is trained to transform noise into new data which resembles a given set of observed/real data. The Wasserstein distance is used to measure the distance between the transformed noise distribution and the distribution of the real data during the training of the generator neural network.

The code contains the implementation of the Wasserstein generative adversarial network (WGAN model) which constitutes the baseline generative model (Xiao et. al). Furthermore, it contains our modifications of the baseline where we implement the _Sinkhorn distance algorithm_ (Sinkhorn model) and _Inexact Proximal Point Method for Exact Optimal Transport_ (IPOT model) for the approximation of the Wasserstein distance. See "gen_models_and_residual_analysis.ipynb" for more details and the analysis of the generative models via a residual analysis.

Additionally, the repository contains the code for the approximation of the TT-metric which when formulated as an optimal transport problem can be approximated by the same Sinkhorn distance algorithm and IPOT algorithm as in the generative models. See "approximation_TT_metric.ipynb" for more details and the corresponding analysis.


# Requirements
- python 3.7+
- pytorch 1.6+
- numpy
- sympy
- matplotlib


# Usage

Clone the repository and install the required packages.


1. Run a generative model on a predefined simulated set of real data (inhom. Poisson process) and perform the residual analysis:

```
usage:python main.py -h --gpu=GPU --learn_rate=LEARN_RATE --model_type=model_type
               --ITERS=ITERS --BATCH_SIZE=BATCH_SIZE

optional arguments:
  -h, --help            show help message for information on arguments and further options and exit
  --gpu                 whether to use GPU for training of neural network if available
  --learn_rate          learning rate in training of generative model
  --model_type          type of generative model: 'WGAN', 'Sinkhorn' or 'IPOT.
  --ITERS               number of training iterations in generative model
  --BATCH_SIZE          training batch size

example:python main.py --gpu=True --learn_rate=0.001 --model_type='Sinkhorn' --ITERS=1000
```
Alternatively: Run the "gen_models_and_residual_analysis.ipynb" notebook for the interactive version of the generative model analysis and further details.


2. Demonstration of TT-metric approximation using predefined simulated data:

```
usage: TT_metric.py
```
Alternatively: Run the "approximation_TT_metric.ipynb" notebook for the interactive analysis of the TT-metric approximation and further details.


# Usage: Google Colab
When using Google Colab, the generative models together with the analysis can be accessed completely in the stand-alone notebook "gen_models_and_residual_analysis_COLAB.ipynb". Similarly, the approximation of the TT-metric and the analysis thereof can be accessed in the stand-alone notebook "approximation_TT_metric_COLAB.ipynb". Furthermore, Google Colab freely provides the usage of GPU processors of accelerated training of the neural networks.


# References
- [1] Xiao et. al: "Wasserstein Learning of Deep Generative Point Process Models"
- [2] Cuturi: "Sinkhorn Distances: Lightspeed Computation of Optimal Transport"
- [3] Xie et. al: "A Fast Proximal Point Method for Computing Exact Wasserstein Distance"
- [4] Müller et. al: "Metrics and barycenters for point pattern data"
