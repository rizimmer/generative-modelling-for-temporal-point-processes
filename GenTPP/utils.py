import numpy as np
import sympy as sy

import math


def intens_func_from_gauss_params(params):
  x = sy.Symbol("x")
  inten = 0
  for i in range(len(params["center"])):
    inten += params["coef"][i]*(sy.exp(-(x-params["center"][i])**2/(2*params['std'][i]**2))/sy.sqrt(2*sy.pi*params['std'][i]**2))
  return inten

def normalcdf(x, mu, sigma):
  return (1.0 + math.erf((x-mu)/ (sigma*math.sqrt(2.0)))) / 2.0

def retrieve_seqs_from_gen(noise_seqs, noise_seqs_lens, generator):
  gen_out, gen_out_lens = generator(noise_seqs, noise_seqs_lens)
  gen_out = np.squeeze(gen_out.detach().cpu().numpy())
  gen_out_lens = np.squeeze(gen_out_lens.detach().cpu().numpy()).astype(int)
  gen_seqs = [seq[:gen_out_lens[i]] for i, seq in enumerate(gen_out)]

  return gen_seqs