import numpy as np
import sympy as sy
import matplotlib.pyplot as plt


from itertools import combinations
from utils import retrieve_seqs_from_gen
from data import (get_intensity,
                  get_residuals_hawkes,
                  get_residuals_inhom_poisson,
                  get_residuals_inhom_poisson_gauss_kernels)





def plot_sample_gen_real_noise(seqs_gen=None, seqs_real=None, seqs_noise=None, intens_func=None, plot_intens_func=True, max_T=100, n_samples=10, size=4):

  # number of plots and positions
  n_plots = 0
  i_gen = 0
  i_real = 0
  i_noise = 0
  if seqs_gen != None:
    n_plots += 1
    i_real += 1
    i_noise += 1
  if seqs_real != None:
    n_plots += 1
    i_noise += 1
  if seqs_noise != None:
    n_plots += 1

  fig, axs = plt.subplots(1, n_plots, figsize=((4/3) * size * n_plots, size), facecolor='w', edgecolor='k')

  if seqs_gen != None:
    plot_sample(axs[i_gen], seqs_gen, n_samples, max_T=max_T, label='Sample of generated sequences', intens_func=intens_func, plot_intens_func=plot_intens_func)
  if seqs_real != None:
    plot_sample(axs[i_real], seqs_real, n_samples, max_T=max_T, label='Sample of real sequences', intens_func=intens_func, plot_intens_func=plot_intens_func)
  if seqs_noise != None:
    plot_sample(axs[i_noise], seqs_noise, n_samples, max_T=max_T, label='Sample of noise sequences', intens_func=intens_func, plot_intens_func=plot_intens_func, is_noise=True)

  plt.show()


def plot_sample(ax, seqs, n_samples, max_T, label='', intens_func=None, plot_intens_func=True, is_noise=False):

  if (intens_func != None) and (plot_intens_func == True):
    x = sy.Symbol("x") 
    intens_func_ = sy.lambdify(x, intens_func, "numpy")
    x_vals = np.arange(0, max_T, 0.1)
    intens_vals = intens_func_(x_vals)
    max_val = np.max(intens_vals)
    gen_ts_inten, gen_inten = get_intensity(seqs, T = max_T, n_t=50, t0=None)

    ax.set_ylim([0, 3.2*max_val+1.5])
    ax.set_xlim([0, max_T])
    ax.set_yticks([])

    if is_noise == False:
      ax.plot(x_vals, intens_vals, linestyle="--", alpha=1, color = 'black', label="true intens.")
      ax.plot(gen_ts_inten, gen_inten, linewidth=2.1, alpha=0.9, label="emp. intens.")
      ax.set_yticks([0, max_val/2, max_val])
      ax.legend(loc="upper right", prop={"size":10}, framealpha=0.8)

    for i in range(1, n_samples):
      if i > 2:
        ax.scatter(seqs[i], ((i-2)/(n_samples-2))*2*max_val*np.ones_like(seqs[i])+1.2*max_val, c="grey", alpha=0.7)

  else:
    for i in range(1, n_samples):
      ax.scatter(seqs[i], i*np.ones_like(seqs[i]), c="grey", alpha=0.7)

    ax.set_xlim([0, max_T])
    ax.set_yticks([])

  ax.set_xlabel(label, fontsize=14, labelpad=15)
  ax.xaxis.set_label_position('top')


def plot_diagnostics(gen_seqs, data_type, max_T, params_hawkes=None, gauss_params=None, intens_func=None, show_intens_func=True, conf_bands=True, n_bins=12, size=4):

  # Residuals and uniform transformation
  if data_type == "hawkes":
    res = get_residuals_hawkes(gen_seqs, params=params_hawkes)

  if data_type == "inhom_poisson":
    res = get_residuals_inhom_poisson(gen_seqs, func_expr=intens_func)
    x = sy.Symbol("x")
    f = sy.lambdify(x, intens_func, "numpy")
    x_vals = np.arange(0,max_T,0.1)
    intens_true = f(x_vals)
    max_val = np.max(intens)
    gen_ts_inten, gen_inten = get_intensity(gen_seqs, T = max_T, n_t=50, t0=None)

  if data_type == "inhom_poisson_gauss_kernels":
    res = get_residuals_inhom_poisson_gauss_kernels(gen_seqs, gauss_params)
    x = sy.Symbol("x")
    f = sy.lambdify(x, intens_func, "numpy")
    x_vals = np.arange(0,max_T,0.1)
    intens_true = f(x_vals)
    max_val = np.max(intens_true)
    gen_ts_inten, gen_inten = get_intensity(gen_seqs, T = max_T, n_t=50, t0=None)

  unif = 1 - np.exp(-res)

  fig, axs = plt.subplots(1, 3, figsize=(4*size, size), facecolor='w', edgecolor='k')

  # Emp intensity vs true intensity if inhom Poisson data
  if (data_type == "inhom_poisson" or data_type == "inhom_poisson_gauss_kernels") and (show_intens_func == True):
    axs[0].set_xlim(0, max_T)
    axs[0].set_ylim(0, 1.7*max_val)
    axs[0].plot(gen_ts_inten, gen_inten, linewidth=2.1, alpha=0.9, label="emp. intens.")
    axs[0].plot(np.arange(0, max_T, 0.1), intens_true, linestyle="--", alpha=1, color = 'black', label="true intens.")
    axs[0].legend(loc="upper right", prop={"size":2*size}, framealpha=0.8)
    axs[0].set_xlabel('Empirical intensity of generated sequences', fontsize=14, labelpad=15)
    axs[0].xaxis.set_label_position('top') 

  # Residuals against exp(1)-distribution
  else:
    range_x = 5
    exp_dist_range = np.linspace(0,range_x,200)
    bins_range = np.linspace(0,range_x,50)
    exp_dist = np.exp(-exp_dist_range)
    weights = (n_bins/range_x) * np.ones_like(res)/float(len(res))
    axs[0].hist(res, bins=n_bins, range=(0,range_x), weights=weights, alpha=0.5, edgecolor='black', linewidth=1.2, label=r"emp. density of residuals")
    axs[0].plot(exp_dist_range, exp_dist, linestyle="--", alpha=0.8, color = 'black', label=r"$f(x) = \exp(-x)$")
    axs[0].set_ylim(0, 1.65)
    axs[0].set_xlim(0, range_x)
    axs[0].legend(loc="upper right", prop={"size":2*size}, framealpha=0.8)
    axs[0].set_xlabel('Residual distribution vs. Exp(1)-distribution', fontsize=14, labelpad=15)
    axs[0].xaxis.set_label_position('top') 

  # Ecdf of U_k with confidence bands
  x = np.sort(unif)
  n = len(unif)
  y = np.arange(1, n+1) / n
  unif_x = np.linspace(0,1)
  unif_y = np.linspace(0,1)
  axs[1].plot(unif_x, unif_y, linestyle="--", alpha=0.8, color = 'black', label=r"$f(x) = x$")
  axs[1].plot(x, y, linewidth=2.1, label=r"e.c.d.f of transf. residuals")
  axs[1].set_xlim(0, 1)
  axs[1].set_ylim(0, 1)
  axs[1].set_xlabel('E.c.d.f of transformed residuals', fontsize=14, labelpad=15)
  axs[1].xaxis.set_label_position('top') 

  if conf_bands == True:
    crit_val_a_95 = 1.358099   # critical value of KS distribution for 95% confidence bands
    crit_val_a_99 = 1.95       # critical value of KS distribution for 99% confidence bands
    K_a_95 = crit_val_a_95/np.sqrt(n)   # value for 95% confidence band
    K_a_99 = crit_val_a_99/np.sqrt(n)   # value for 95% confidence band

    axs[1].plot(unif_x, unif_y+K_a_95, linestyle=":", alpha=0.2, color = 'black')
    axs[1].plot(unif_x, unif_y-K_a_95, linestyle=":", alpha=0.2, color = 'black')
    axs[1].plot(unif_x, unif_y+K_a_99, linestyle=":", alpha=0.2, color = 'black')
    axs[1].plot(unif_x, unif_y-K_a_99, linestyle=":", alpha=0.2, color = 'black')

    axs[1].fill_between(unif_x, unif_y+K_a_95, unif_y-K_a_95, alpha=0.2, linewidths=0, color = 'black')  #, label=r"95% conf. band")
    axs[1].fill_between(unif_x, unif_y+K_a_99, unif_y+K_a_95, alpha=0.1, linewidths=0, color = 'black')  #, label=r"99% conf. band")
    axs[1].fill_between(unif_x, unif_y-K_a_99, unif_y-K_a_95, alpha=0.1, linewidths=0, color = 'black')

    axs[1].legend(loc="lower right", prop={"size":2*size}, framealpha=0.8)

  # Scatter plot of (U_k, U_k+1)
  axs[2].scatter(unif[1:], unif[:-1], alpha=0.4)
  axs[2].set_xlim(0, 1)
  axs[2].set_ylim(0, 1)
  axs[2].set_xlabel('Scatter plot of transformed residual pairs', fontsize=14, labelpad=15)
  axs[2].xaxis.set_label_position('top')

  plt.show()

  corcoef = np.round(np.corrcoef(unif[1:], unif[:-1])[0,1], 3)
  # print("Correlation of U_k and U_k+1: ", corcoef)


def plot_residual_analysis_compare_models(noise_tensor, noise_len_tensor, generators, data_type, max_T, intens_func=None, params=None, n_bins=12, conf_bands=True, plot_inten=True, size=6):
  # perform residual analysis on generated sequences for each generator in generators
  
  data_plots = []
  results = []
  unifs = []
  n_models = len(generators)

  with torch.no_grad():
    for generator in generators:
      gen_out, gen_out_len = generator(noise_tensor, noise_len_tensor)
      gen_out = np.squeeze(gen_out.detach().cpu().numpy())
      gen_out_len = np.squeeze(gen_out_len.detach().cpu().numpy()).astype(int)
      data_plot = [seq[:gen_out_len[i]] for i, seq in enumerate(gen_out)]
      data_plots.append(data_plot)

      # Residuals and uniform transformation
      if data_type == "hawkes":
        res = get_residuals_hawkes(data_plot, params=params_hawkes)
        results.append(res)
      if data_type == "self_correcting":
        res = get_residuals_self_correcting(data_plot)
        results.append(res)
      if data_type == "inhom_poisson":
        res = get_residuals_inhom_poisson(data_plot, func_expr=intens_func)
        results.append(res)

      unif = 1 - np.exp(-res)
      unifs.append(unif)

  if plot_inten == True:
    fig, axs = plt.subplots(3, n_models, figsize=(4*size, 3*size), facecolor='w', edgecolor='k') 
  else:
    fig, axs = plt.subplots(2, n_models, figsize=(4*size, 2*size), facecolor='w', edgecolor='k')

  for j in range(n_models):

    if plot_inten == True:
      # Emp intensity of generated against data intensity
      x = sy.Symbol("x")
      func = sy.lambdify(x, intens_func, "numpy")
      x_vals = np.arange(0, max_T, 0.1)
      intens = func(x_vals)
      max_val = np.max(intens)

      gen_ts_inten, gen_inten = get_intensity(data_plots[j], T = max_T, n_t=30, t0=None)

      axs[2,j].set_xlim(0, max_T)
      axs[2,j].set_ylim(0, 1.3*max_val)
      axs[2,j].plot(gen_ts_inten, gen_inten, linewidth=2.1, alpha=0.9, label="emp. intens.")
      axs[2,j].plot(np.arange(0, max_T, 0.1), intens, linestyle="--", alpha=1, color = 'black', label="true intens.")
      axs[2,j].legend(loc="upper right", prop={"size":3*size}, framealpha=0.8)
      axs[2,j].tick_params(axis='both', labelsize=16)

    # Residuals against exp(1)-distribution
    range_x = 5
    exp_dist_range = np.linspace(0,range_x,200)
    bins_range = np.linspace(0,range_x,50)
    exp_dist = np.exp(-exp_dist_range)
    weights = (n_bins/range_x) * np.ones_like(results[j])/float(len(results[j]))
    axs[0,j].hist(results[j], bins=n_bins, range=(0,range_x), weights=weights, alpha=0.5, edgecolor='black', linewidth=1.2, label=r"emp. density of $S$")
    axs[0,j].plot(exp_dist_range, exp_dist, linestyle="--", alpha=1, color = 'black', label=r"$f(x) = \exp(-x)$")
    axs[0,j].set_ylim([0, 1.45])
    axs[0,j].set_xlim([0, range_x])
    axs[0,j].legend(loc="upper right", prop={"size":3*size}, framealpha=0.8)
    axs[0,j].tick_params(axis='both', labelsize=2*size)

    # Ecdf of U_k with confidence bands
    n = len(unifs[j])
    x = np.sort(unifs[j])
    y = np.arange(1, n+1) / n
    unif_x = np.linspace(0,1)
    unif_y = np.linspace(0,1)

    axs[1,j].plot(unif_x, unif_y, linestyle="--", alpha=0.8, color = 'black', label=r"$f(x) = x$")
    axs[1,j].plot(x, y, linewidth=2.1, label=r"emp. c.d.f of $U$")
    axs[1,j].set_xlim([0, 1])
    axs[1,j].set_ylim([0, 1])

    if conf_bands == True:
      crit_val_a_95 = 1.358099   # critical value of KS distribution for 95% confidence bands
      crit_val_a_99 = 1.95       # critical value of KS distribution for 99% confidence bands
      K_a_95 = crit_val_a_95/np.sqrt(n)   # value for 95% confidence band
      K_a_99 = crit_val_a_99/np.sqrt(n)   # value for 95% confidence band

      axs[1,j].plot(unif_x, unif_y+K_a_95, linestyle=":", alpha=0.2, color = 'black')
      axs[1,j].plot(unif_x, unif_y-K_a_95, linestyle=":", alpha=0.2, color = 'black')
      axs[1,j].plot(unif_x, unif_y+K_a_99, linestyle=":", alpha=0.2, color = 'black')
      axs[1,j].plot(unif_x, unif_y-K_a_99, linestyle=":", alpha=0.2, color = 'black')

      axs[1,j].fill_between(unif_x, unif_y+K_a_95, unif_y-K_a_95, alpha=0.2, linewidths=0, color = 'black')  #, label=r"95% conf. band")
      axs[1,j].fill_between(unif_x, unif_y+K_a_99, unif_y+K_a_95, alpha=0.1, linewidths=0, color = 'black')  #, label=r"99% conf. band")
      axs[1,j].fill_between(unif_x, unif_y-K_a_99, unif_y-K_a_95, alpha=0.1, linewidths=0, color = 'black')

      axs[1,j].legend(loc="lower right", prop={"size":3*size}, framealpha=0.8)
      axs[1,j].tick_params(axis='both', labelsize=2*size)

  fontsize = 4*size
  labelpad = 3*size
  axs[0,0].set_xlabel('WGAN', fontsize=fontsize, labelpad=labelpad)
  axs[0,0].xaxis.set_label_position('top')
  axs[0,1].set_xlabel('Sinkhorn distance', fontsize=fontsize, labelpad=labelpad)
  axs[0,1].xaxis.set_label_position('top')
  axs[0,2].set_xlabel('IPOT', fontsize=fontsize, labelpad=labelpad)
  axs[0,2].xaxis.set_label_position('top')


def plot_residual_analysis_compare_models(noise_tensor, noise_len_tensor, generators, data_type, max_T, intens_func=None, params=None, n_bins=12, conf_bands=True, plot_inten=True, size=6):
  # perform residual analysis on generated sequences for each generator in generators
  
  data_plots = []
  results = []
  unifs = []
  n_models = len(generators)

  with torch.no_grad():
    for generator in generators:
      gen_out, gen_out_len = generator(noise_tensor, noise_len_tensor)
      gen_out = np.squeeze(gen_out.detach().cpu().numpy())
      gen_out_len = np.squeeze(gen_out_len.detach().cpu().numpy()).astype(int)
      data_plot = [seq[:gen_out_len[i]] for i, seq in enumerate(gen_out)]
      data_plots.append(data_plot)

      # Residuals and uniform transformation
      if data_type == "hawkes":
        res = get_residuals_hawkes(data_plot, params=params_hawkes)
        results.append(res)
      if data_type == "self_correcting":
        res = get_residuals_self_correcting(data_plot)
        results.append(res)
      if data_type == "inhom_poisson":
        res = get_residuals_inhom_poisson(data_plot, func_expr=intens_func)
        results.append(res)

      unif = 1 - np.exp(-res)
      unifs.append(unif)

  if plot_inten == True:
    fig, axs = plt.subplots(3, n_models, figsize=(4*size, 3*size), facecolor='w', edgecolor='k') 
  else:
    fig, axs = plt.subplots(2, n_models, figsize=(4*size, 2*size), facecolor='w', edgecolor='k')

  for j in range(n_models):

    if plot_inten == True:
      # Emp intensity of generated against data intensity
      x = sy.Symbol("x")
      func = sy.lambdify(x, intens_func, "numpy")
      x_vals = np.arange(0, max_T, 0.1)
      intens = func(x_vals)
      max_val = np.max(intens)

      gen_ts_inten, gen_inten = get_intensity(data_plots[j], T = max_T, n_t=30, t0=None)

      axs[2,j].set_xlim(0, max_T)
      axs[2,j].set_ylim(0, 1.3*max_val)
      axs[2,j].plot(gen_ts_inten, gen_inten, linewidth=2.1, alpha=0.9, label="emp. intens.")
      axs[2,j].plot(np.arange(0, max_T, 0.1), intens, linestyle="--", alpha=1, color = 'black', label="true intens.")
      axs[2,j].legend(loc="upper right", prop={"size":3*size}, framealpha=0.8)
      axs[2,j].tick_params(axis='both', labelsize=16)

    # Residuals against exp(1)-distribution
    range_x = 5
    exp_dist_range = np.linspace(0,range_x,200)
    bins_range = np.linspace(0,range_x,50)
    exp_dist = np.exp(-exp_dist_range)
    weights = (n_bins/range_x) * np.ones_like(results[j])/float(len(results[j]))
    axs[0,j].hist(results[j], bins=n_bins, range=(0,range_x), weights=weights, alpha=0.5, edgecolor='black', linewidth=1.2, label=r"emp. density of $S$")
    axs[0,j].plot(exp_dist_range, exp_dist, linestyle="--", alpha=1, color = 'black', label=r"$f(x) = \exp(-x)$")
    axs[0,j].set_ylim([0, 1.45])
    axs[0,j].set_xlim([0, range_x])
    axs[0,j].legend(loc="upper right", prop={"size":3*size}, framealpha=0.8)
    axs[0,j].tick_params(axis='both', labelsize=2*size)

    # Ecdf of U_k with confidence bands
    n = len(unifs[j])
    x = np.sort(unifs[j])
    y = np.arange(1, n+1) / n
    unif_x = np.linspace(0,1)
    unif_y = np.linspace(0,1)

    axs[1,j].plot(unif_x, unif_y, linestyle="--", alpha=0.8, color = 'black', label=r"$f(x) = x$")
    axs[1,j].plot(x, y, linewidth=2.1, label=r"emp. c.d.f of $U$")
    axs[1,j].set_xlim([0, 1])
    axs[1,j].set_ylim([0, 1])

    if conf_bands == True:
      crit_val_a_95 = 1.358099   # critical value of KS distribution for 95% confidence bands
      crit_val_a_99 = 1.95       # critical value of KS distribution for 99% confidence bands
      K_a_95 = crit_val_a_95/np.sqrt(n)   # value for 95% confidence band
      K_a_99 = crit_val_a_99/np.sqrt(n)   # value for 95% confidence band

      axs[1,j].plot(unif_x, unif_y+K_a_95, linestyle=":", alpha=0.2, color = 'black')
      axs[1,j].plot(unif_x, unif_y-K_a_95, linestyle=":", alpha=0.2, color = 'black')
      axs[1,j].plot(unif_x, unif_y+K_a_99, linestyle=":", alpha=0.2, color = 'black')
      axs[1,j].plot(unif_x, unif_y-K_a_99, linestyle=":", alpha=0.2, color = 'black')

      axs[1,j].fill_between(unif_x, unif_y+K_a_95, unif_y-K_a_95, alpha=0.2, linewidths=0, color = 'black')  #, label=r"95% conf. band")
      axs[1,j].fill_between(unif_x, unif_y+K_a_99, unif_y+K_a_95, alpha=0.1, linewidths=0, color = 'black')  #, label=r"99% conf. band")
      axs[1,j].fill_between(unif_x, unif_y-K_a_99, unif_y-K_a_95, alpha=0.1, linewidths=0, color = 'black')

      axs[1,j].legend(loc="lower right", prop={"size":3*size}, framealpha=0.8)
      axs[1,j].tick_params(axis='both', labelsize=2*size)

  fontsize = 4*size
  labelpad = 3*size
  axs[0,0].set_xlabel('WGAN', fontsize=fontsize, labelpad=labelpad)
  axs[0,0].xaxis.set_label_position('top')
  axs[0,1].set_xlabel('Sinkhorn distance', fontsize=fontsize, labelpad=labelpad)
  axs[0,1].xaxis.set_label_position('top')
  axs[0,2].set_xlabel('IPOT', fontsize=fontsize, labelpad=labelpad)
  axs[0,2].xaxis.set_label_position('top')


def plot_gen_samples_compare_models(noise_tensor, noise_len_tensor, generators, max_T, size=3):
  # for each generator in generators plot a sample of generated sequences

  data_plots = []
  results = []
  unifs = []
  n_models = len(generators)

  with torch.no_grad():
    for generator in generators:
      gen_out, gen_out_len = generator(noise_tensor, noise_len_tensor)
      gen_out = np.squeeze(gen_out.detach().cpu().numpy())
      gen_out_len = np.squeeze(gen_out_len.detach().cpu().numpy()).astype(int)
      data_plot = [seq[:gen_out_len[i]] for i, seq in enumerate(gen_out)]
      data_plots.append(data_plot)

  fig, axs = plt.subplots(1, n_models, figsize=(8*size, size), facecolor='w', edgecolor='k')
  fig.subplots_adjust(hspace = 0.15, wspace= 0.1)

  for j in range(3):

    axs[j].set_xlim(0, max_T)
    axs[j].set_ylim(0, 8)
    axs[j].set_yticks([])

    for i in range(1, 8):
      axs[j].scatter(data_plots[j][i], i*np.ones_like(data_plots[j][i]), c="grey", alpha=0.7)

  fontsize = 8*size
  labelpad = 5*size
  axs[0].set_xlabel('WGAN', fontsize=fontsize, labelpad=labelpad)
  axs[0].xaxis.set_label_position('top')
  axs[1].set_xlabel('Sinkhorn distance', fontsize=fontsize, labelpad=labelpad)
  axs[1].xaxis.set_label_position('top')
  axs[2].set_xlabel('IPOT', fontsize=fontsize, labelpad=labelpad)
  axs[2].xaxis.set_label_position('top')


def plot_metric_comparision_vary_noise_mean_intens(val_data_list, generators, intens_func, max_T, size=6):
  # plot the empirical intensity of generated sequences for varying mean intensity of noise process
  # and compare results of Xiao-metric and TT-metric

  data_plots = []

  with torch.no_grad():
    for i, generator in enumerate(generators):
      if i == 0 or i == 1:
        noise_tensor = val_data_list[0].batch_noise_seqs
        noise_len_tensor = val_data_list[0].batch_noise_seqs_lens
      if i == 2 or i == 3:
        noise_tensor = val_data_list[1].batch_noise_seqs
        noise_len_tensor = val_data_list[1].batch_noise_seqs_lens
      if i == 4 or i == 5:
        noise_tensor = val_data_list[2].batch_noise_seqs
        noise_len_tensor = val_data_list[2].batch_noise_seqs_lens

      data_plot = retrieve_seqs_from_gen(noise_tensor, noise_len_tensor, generator)
      data_plots.append(data_plot)

  fig, axs = plt.subplots(2, 3, figsize=(9*size, 2*size), facecolor='w', edgecolor='k')

  # Emp intensity of generated against data intensity
  x = sy.Symbol("x")
  func = sy.lambdify(x, intens_func, "numpy")
  x_vals = np.arange(0, max_T, 0.1)
  intens = func(x_vals)
  max_val = np.max(intens)

  labels = ['$\lambda_{noise} = \lambda_{observed}$', '$\lambda_{noise} > \lambda_{observed}$', '$\lambda_{noise} < \lambda_{observed}$']

  for i in range(2):
    for j in range(3):

      gen_ts_inten, gen_inten = get_intensity(data_plots[2*j+i], T = max_T, n_t=20, t0=None)

      axs[i,j].set_xlim(0, max_T)
      axs[i,j].set_ylim(0, 1.7*max_val)
      axs[i,j].plot(gen_ts_inten, gen_inten, linewidth=2.1, alpha=0.9, label="emp. intens.")
      axs[i,j].plot(np.arange(0, max_T, 0.1), intens, linestyle="--", alpha=1, color = 'black', label="true intens.")
      if i == 0:
        leg = axs[i,j].legend(loc="upper right", title="Metric: Xiao", framealpha=0.8, bbox_to_anchor=(0.99,0.99))
        leg._legend_box.align = "left"
      if i == 1:
        leg = axs[i,j].legend(loc="upper right", title="Metric: TT", framealpha=0.8, bbox_to_anchor=(0.99,0.99))
        leg._legend_box.align = "left"

      axs[i,j].tick_params(axis='both')

      fontsize = 6*size
      labelpad = 3*size

      if i == 0:
        axs[i,j].set_xlabel(labels[j], fontsize=fontsize, labelpad=labelpad)
        axs[i,j].xaxis.set_label_position('top')


def plot_min_xiao_dist_gen_to_obs(gen_seqs, obs_seqs, max_T, size=6):
  # for each generated sequence in gen_seqs find the closest neighbour (w.r.t. Xiao-distance) in the
  # set of observed sequences in obs_seqs and plot the two sequences side-by-side
  
  obs_seqs_len = [len(s) for s in obs_seqs]
  gen_seqs_len = [len(s) for s in gen_seqs]

  max_seq_len = max(max(gen_seqs_len), max(obs_seqs_len))

  array_obs_seqs = np.ones((len(obs_seqs), max_seq_len)) * max_T
  for i in range(len(obs_seqs)):
    array_obs_seqs[i][:obs_seqs_len[i]] =  obs_seqs[i]

  closest_in_obs = [] # index of closest seq in obs_seqs for each seq in gen_seqs
  dist_closest_in_obs = []

  for i in range(len(gen_seqs)):

    array_gen_seqs = np.ones((len(obs_seqs), max_seq_len)) * max_T

    for j in range(len(obs_seqs)):
      array_gen_seqs[j][:gen_seqs_len[i]] =  gen_seqs[i]

    dists = np.abs(array_obs_seqs - array_gen_seqs)
    dists = np.sum(dists, axis=-1).squeeze()

    ind_closest_seq = dists.argsort()[0]
    dist_closest_seq = dists[ind_closest_seq]

    closest_in_obs.append(ind_closest_seq)
    dist_closest_in_obs.append(dist_closest_seq)

  fig, axs = plt.subplots(1, 2, figsize=(4*size, size), facecolor='w', edgecolor='k')

  axs[0].set_xlim(0, max_T)
  axs[0].set_yticks([])

  axs[1].set_xlim(0, max_T)
  axs[1].set_yticks([])

  n_seqs = min(min(len(gen_seqs), 6), len(obs_seqs))
  for i in range(n_seqs):
    axs[0].scatter(gen_seqs[i], i*np.ones_like(gen_seqs[i]), c="grey", alpha=0.7)

  for i in range(n_seqs):
    axs[1].scatter(obs_seqs[closest_in_obs[i]], i*np.ones_like(obs_seqs[closest_in_obs[i]]), c="grey", alpha=0.7)

  fontsize = 4*size
  labelpad = 2*size
  axs[0].set_xlabel('Generated sequences', fontsize=fontsize, labelpad=labelpad)
  axs[0].xaxis.set_label_position('top')
  axs[1].set_xlabel('Closest neighbour in real sequences', fontsize=fontsize, labelpad=labelpad)
  axs[1].xaxis.set_label_position('top')